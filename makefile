CC=gcc
CFLAGS=-pthread -I.
DEPS=bridge_construct.h


all: main clean 

main: bridge.o bridge_construct.o
	$(CC) -o bridge bridge.o bridge_construct.o $(CFLAGS)

bridge.o:  bridge.c 
	$(CC) -c bridge.c $(DEPS) $(CFLAGS)

bridge_construct.o: bridge_construct.c $(DEPS)
	$(CC) -c bridge_construct.c $(CFLAGS)

clean:
	rm -f *.o *.gch
