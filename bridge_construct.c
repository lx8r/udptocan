#include "bridge_construct.h"

int BridgeInit( const char *serverPort, 
				const char *canDevName, 
				const char *clientIP, 
				const char *clientPort,
				bridge_t *bridgeInfo){
	int socketCAN, socketUDP;
	struct sockaddr_in peer_addr;
	
	
	socketUDP = createSocketUDP(serverPort);
	socketCAN = createSocketCAN(canDevName);
	
	if( (socketUDP<0)||(socketCAN<0) ){	
		fprintf(stderr, "Bridge for %s does not created!\n\n", canDevName);
		return -1;
	}
	
	memset(&peer_addr, 0, sizeof(struct sockaddr_in));
	peer_addr.sin_family = AF_INET;
	peer_addr.sin_port   = htons(atoi(clientPort));
	inet_pton(AF_INET, clientIP, &(peer_addr.sin_addr));
	
	bridgeInfo->socketCAN = socketCAN;
	bridgeInfo->socketUDP = socketUDP;
	bridgeInfo->peer_addr = peer_addr;
	bridgeInfo->peer_addr_len = sizeof(struct sockaddr_in);
	snprintf(bridgeInfo->name, DEV_NAME_SIZE, "Bridge:%s", canDevName);
	
	
	printf("%s \nServer port:%s \nClient ip:%s port:%s\n\n", 
			bridgeInfo->name, serverPort, clientIP, clientPort);
	return 1;
}
				

int msgToFrame(const char *msg, struct can_frame *retFrame){
	uint64_t id;
	uint8_t dlc=0;
	uint8_t tmpByte;
	uint8_t tmpBuf[8];
	uint64_t tmpData = 0;	
	
	memset(retFrame, 0, sizeof(struct can_frame));
	sscanf(msg, "ID=%lx DATA=%lX", &id, &tmpData);
	
	if(id > 0x7FF)					//Max 11_bit ID = 0x7FF
		id = id|CAN_EFF_FLAG;		//Extended 29_bit ID
	else if(id > 0x1FFFFFFF)
		return -1;					//Bad ID
	
	if(tmpData >= MAX_UINT64)
		return -2;					//Invalid data. Length > 8 bytes


	for(int i=56, dlc=0; i>=0 ; i-=8){
		tmpByte = (tmpData>>i)&0xFF; //take every byte from rx DATA

		if( (tmpByte>0)||(dlc>0) )	//beginning of data OR data sequence started
				tmpBuf[dlc++] = tmpByte;
	}
	
	
	retFrame->can_id  = id;
	retFrame->can_dlc = dlc;
	for(int i=0;i<dlc;i++){
		retFrame->data[i] = tmpBuf[i];
	}
	
	return dlc;
}

int frameToMsg(const struct can_frame *frame, char *retMsg){
	int nBytes; 
	char cData[5];
	
	if(frame->can_id&CAN_RTR_FLAG)
		sprintf(retMsg, "Remote frame. ID=%X", frame->can_id);
	else if(frame->can_id&CAN_ERR_FLAG)
		sprintf(retMsg, "Error frame. ID=%X", frame->can_id);
	else{
		sprintf(retMsg, "ID=%X DLC=%d", frame->can_id, frame->can_dlc);
		
		if(frame->can_dlc > 0)
			strcat(retMsg, " DATA=");
			
		for(int i=0; i<frame->can_dlc; i++){
			snprintf(cData, sizeof(cData), "%02X ", frame->data[i]);		
			strcat(retMsg, cData);			//Adding "DATA" bytes to Msg
		}
	}
	strcat(retMsg, "\n");
	
	nBytes = strlen(retMsg) + 1;
	if(nBytes > BUF_SIZE){					//Paranoid check
		fprintf(stderr, "Buffer %d too small for CAN frame [%d bytes]!\n", BUF_SIZE, nBytes);
		exit(EXIT_FAILURE);
		return 0;
	}
	
	return nBytes;
}

int readCAN(const bridge_t *br, struct can_frame *retFrame){
	int nBytes;
	
	memset(retFrame, 0, sizeof(struct can_frame));
	nBytes = read(br->socketCAN, retFrame, sizeof(struct can_frame));
	
	if (nBytes == -1)			
		return -1;				// Ignore failed request. EAGAIN error. 
	
	printf("%s. CAN frame recieved\n", br->name);
	
	return nBytes;
}

int sendCAN(const bridge_t *br, const struct can_frame *frame){
	int nBytes;
	
	nBytes = write(br->socketCAN, frame, sizeof(struct can_frame));
	if (nBytes < 0){
		fprintf(stderr, "%s. Error at sending CAN\n", br->name);
		return -2;
	}
	
	printf("%s. CAN frame sent\n", br->name);
	return nBytes;	
}

int readUDP(const bridge_t *br, char *buff, int buffSize){
	int nBytes;
	struct sockaddr_in peer_addr;
	socklen_t peer_addr_len;
	
	peer_addr_len = sizeof(struct sockaddr_in);
	nBytes = recvfrom(br->socketUDP, buff, buffSize, 0,
		   (struct sockaddr *) &peer_addr, &peer_addr_len);
	if (nBytes == -1)			
		return -1;					//Ignore failed request. EAGAIN error. 


	printf("%s. Read %d bytes from UDP\n", br->name, nBytes);
		
	return nBytes;
}

int sendUDP(const bridge_t *br, const char *buff, const int bytesToSend){
	int nBytes;
	
	nBytes = sendto(br->socketUDP, buff, bytesToSend, 0,
						(struct sockaddr *) &(br->peer_addr),
						br->peer_addr_len);
	
	if (nBytes != bytesToSend){
		fprintf(stderr, "%s. Error at sending UDP\n", br->name);
		return -2;
	}
	
	printf("%s .Wrote %d bytes to UDP\n", br->name, nBytes);
	return nBytes;
}

int createSocketUDP(const char * hostPort){
	int socketFD;
	struct sockaddr_in bindAddr;

	socketFD = socket(AF_INET, SOCK_DGRAM|SOCK_NONBLOCK, 0);
	if (socketFD <= 0){ 
		fprintf(stderr, "Error while opening UDP socket\n");
		return -1;
	}
	
	memset(&bindAddr, 0, sizeof(struct sockaddr_in)); 
	bindAddr.sin_family = AF_INET;
    bindAddr.sin_port = htons(atoi(hostPort));
    bindAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (0 > bind(socketFD,(struct sockaddr *)&bindAddr, sizeof bindAddr)) {
		fprintf(stderr, "Error while binding UDP socket\n");
		return -2;
	}
	
	return socketFD;
}

int createSocketCAN(const char *canDevName){
	int canSocket;
	struct sockaddr_can addr;
	struct ifreq ifr;

	canSocket = socket(PF_CAN, SOCK_RAW|SOCK_NONBLOCK, CAN_RAW);
	if(canSocket < 0) {
		fprintf(stderr, "Error while opening CAN socket\n");
		return -1;
	}

	strcpy(ifr.ifr_name, canDevName);
	ioctl(canSocket, SIOCGIFINDEX, &ifr);
	
	if(ifr.ifr_ifindex <= 0){
		fprintf(stderr, "Wrong device name: %s\n", canDevName);
		return -2;
	}
	
	addr.can_family  = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;
	//printf("%s at index %d\n", canDevName, ifr.ifr_ifindex);
	
	if(0 > bind(canSocket, (struct sockaddr *)&addr, sizeof addr)) {
		fprintf(stderr, "Error in CAN socket bind\n");
		return -3;
	}
	
	ioctl(canSocket, SIOCGIFFLAGS, &ifr);
    ifr.ifr_flags |= (IFF_UP | IFF_RUNNING);	//try to turn UP interface
    ioctl(canSocket, SIOCSIFFLAGS, &ifr);		//privileged  operation

	return canSocket;
}	
