#ifndef _bridge_construct_h
#define _bridge_construct_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <netdb.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <net/if.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <pthread.h>



#define BUF_SIZE 500
#define DEV_NAME_SIZE 16
#define MAX_UINT64 0xFFFFFFFFFFFFFFFF

struct Bridge{
	int socketCAN;
	int socketUDP;
	
	struct sockaddr_in peer_addr;
	socklen_t peer_addr_len;
	
	char name[DEV_NAME_SIZE];
};
typedef struct Bridge bridge_t; 

void* BridgeWorker(void* argument);
int BridgeInit( const char *serverPort, 
				const char *canDevName, 
				const char *clientIP, 
				const char *clientPort,
				bridge_t *retBridgeInfo);

int msgToFrame(const char *msg, struct can_frame *retFrame);
int frameToMsg(const struct can_frame *frame, char *retMsg);

int createSocketUDP(const char *hostPort);
int readUDP(const bridge_t *br, char *retBuff, int buffSize);
int sendUDP(const bridge_t *br, const char *buff, const int bytesToSend);

int createSocketCAN(const char *canDevName);
int readCAN(const bridge_t *br, struct can_frame *retFrame);
int sendCAN(const bridge_t *br, const struct can_frame *frame);

#endif
