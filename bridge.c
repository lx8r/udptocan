#include <pthread.h>
#include "bridge_construct.h"

#define BRIDGES_NUM 3

int main(int argc, char *argv[]) {
	const char clientIP[] = "192.168.1.126";
	const char cliPorts[BRIDGES_NUM][6] = {"41487", "41488", "41449"};
	const char srvPorts[BRIDGES_NUM][6] = {"42487", "42488", "42449"};
	const char   canDev[BRIDGES_NUM][6] = {"vcan0", "vcan1", "vcan2"};
	
	bridge_t bridges[BRIDGES_NUM];
	pthread_t threads[BRIDGES_NUM];


	for(int i=0; i<BRIDGES_NUM; i++){
		if(0 < BridgeInit(srvPorts[i], canDev[i], clientIP, cliPorts[i], &bridges[i]))
			pthread_create(&threads[i], NULL, BridgeWorker, &bridges[i]);
	}

	// wait for each thread to complete
	//*wait forever
	for(int i=0; i<BRIDGES_NUM; i++){
		pthread_join(threads[i], NULL);
	}
	
	return 0;
}

/*----------------------------------------------------*/

void* BridgeWorker(void* argument) {
	int retValue;
	int nBytesMsg;
	char buffer[BUF_SIZE];
	struct can_frame frame;
	
	bridge_t *pBridge = (bridge_t *) argument;
	
	for (;;) {
										
		retValue = readUDP(pBridge, buffer, BUF_SIZE);
		if(retValue > 0){							
			if(msgToFrame(buffer, &frame) >= 0)		//UDP msg recieved
				sendCAN(pBridge, &frame);			//UDP msg correct
		}
		
		retValue = readCAN(pBridge, &frame);
		if(retValue > 0){
			nBytesMsg = frameToMsg(&frame, buffer);	//CAN msg recieved
			sendUDP(pBridge, buffer, nBytesMsg);
		}
	}
	
	return NULL;
}
